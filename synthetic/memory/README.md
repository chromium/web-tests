# Memory Synthetic Test

Simple workload to allocate memory.

Workload can be used interactively, or with URL params to control initial
behaviour upon pageload.
